def multiplication_table(number):
    print(f"multiplication number: {number}")
    for i in range(number):
        for j in range(number):
            print(i*number + j + 1, end="\t")
        print()

multiplication_table(3)
multiplication_table(6)