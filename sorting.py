print("========== PROGRAM SORTING ==========")
print()

list_arr = input("Masukkan List Input (kasih spasi angka beda) : \n")
list_arr = list_arr.split(" ")

print("Output :")
number = 0
for i in range (len(list_arr) - 1):
    for j in range(len(list_arr) - i - 1):
        if list_arr[j] > list_arr[j+1]:
            number += 1
            temp = list_arr[j]
            list_arr[j] = list_arr[j+1]
            list_arr[j+1] = temp
            result_temp = " ".join(list_arr)
            print(f"{number}. [{list_arr[j]}, {list_arr[j+1]}] -> {result_temp}")