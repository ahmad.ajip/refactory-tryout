def string_manipulation(word):
    list_product = ["Burger", "Fries", "Chicken", "Pizza", "Sandwich", "Onionrings", "Milkshake", "Coke"]
    result = ""

    for product in list_product:
        while (product.lower() in word):
            result += product.capitalize() + " "
            word = word.replace(product.lower(), "", 1)

    print(result)

string_manipulation("milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza")
string_manipulation("pizzachickenfriesburgercokemilkshakefriessandwich")