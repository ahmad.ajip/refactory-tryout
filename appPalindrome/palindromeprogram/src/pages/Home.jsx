import React, { Component } from "react";

class Home extends Component {
  state = {word: "", palindrome: ""}
  
  changeInput = (el) => {
    this.setState({ word: el.target.value });
  };

  handleSubmit = () => {
    const { word } = this.state;
    var wordLowerCase = word.toLowerCase()
    var resultWord = ""
    var reverseWord = ""

    for (var i = wordLowerCase.length-1; i >= 0; i--){
      if (wordLowerCase[i].charCodeAt()>96 && wordLowerCase[i].charCodeAt()<123){
        resultWord = wordLowerCase[i] + resultWord
        reverseWord = reverseWord + wordLowerCase[i]
      }
    }

    console.log("result", resultWord)
    console.log("reverse", reverseWord)

    if (word === ""){
      this.setState({palindrome: ""})
    }
    else if (resultWord === reverseWord){
      this.setState({palindrome: "PALINDROME"})
    }
    else {
      this.setState({palindrome: "BUKAN PALINDROME"})
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-10 col-xl-9 mx-auto">
              <div className="card card-signin flex-row my-5">
                <div className="card-img-left d-none d-md-flex"></div>
                <div className="card-body my-auto">
                  <h2 className="card-title text-center">CHECK WORD IS PALINDROME</h2>
                  <form
                    className="form-palindrome"
                    onSubmit={(el) => el.preventDefault()}
                  >
                    <div className="form-label-group">
                      <input
                        type="text"
                        name="palindrome"
                        className="form-control"
                        placeholder="Input Word Here"
                        onChange={(el) => this.changeInput(el)}
                        onKeyUp={() => this.handleSubmit()}
                        required
                        autoFocus
                      />
                    </div>

                    <p
                      className="btn btn-lg btn-secondary btn-block text-uppercase mt-3"
                      type="submit"
                      onClick={() => this.handleSubmit()}
                    >
                      Palindrome Check
                    </p>
                    <h3 className="card-title text-center">{this.state.palindrome}</h3>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
  
  export default Home;