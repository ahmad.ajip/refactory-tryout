def hello(name="World"):
    if name == "":
        print(f"hello \"\" \t => \"Hello, World!\"")
    else:
        if name == "World":
            print(f"hello  \t \t => \"Hello, {name.capitalize()}!\"")
        else:
            print(f"hello {name.capitalize()} \t => \"Hello, {name.capitalize()}!\"")

hello("john")
hello("aliCE")
hello()
hello("")